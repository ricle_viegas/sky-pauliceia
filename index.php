<?php
include 'detectdevice.php';

$empreendimentoId = 141;

session_start();
$device = detect_mobile();
$parametros = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null;
$origemUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
$origem = parse_url($origemUrl);
$origemHost = isset($origem['host']) ? $origem['host']: null;

if(!is_null($origemHost) && is_null($_SESSION['origem'])){
  $_SESSION['origem'] = $origemHost;
}else if($_SESSION['origem'] != $origemHost){
  $_SESSION['origem'] = $origemHost;
}

if(is_null($origemHost) || $_SERVER['SERVER_NAME'] == $origemHost){
    $origemHost = 'direct';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Aluguel Investido TPA</title>
<meta name="description" content="Use o valor da locação como entrada na compra do seu Apê!" />
<link rel="icon" type="image/png"   href="favicon.png">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,700;1,500;1,700&display=swap" rel="stylesheet">
<link href="css/lightslider.css" rel="stylesheet" type="text/css">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/responsivo.css" rel="stylesheet" type="text/css">
<link href="js/lightbox/themes/carbono/jquery.lightbox.css" rel="stylesheet" type="text/css">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41053658-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-41053658-20');
</script>



<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2140704122845774');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2140704122845774&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->




<script language="javascript" type="text/javascript">
	<!--
	function popitup(url) {
		newwindow=window.open(url,'name','height=600,width=550');
		if (window.focus) {newwindow.focus()}
		return false;
	}

	// -->
</script>




</head>
<body>


<div class="head col12">
	<h1 class="logo col-left"><a class="" href="http://tpaempreendimentos.com.br/"><img src="img/tpa.svg" alt="Tpa Empreendimentos"/></a></h1>
	<div class="col-right">
		<a class="box col-left shake" href="#duvidas"><span>Dúvidas</span> <img src="img/ico-duvidas.svg" alt="Dúvidas"/></a>
		<a class="box col-left shake tele"  target="_blank"><span>Fale conosco</span><img src="img/ico-whats.svg" alt="Icone Whatsapp"/></a>
	</div>
</div>
<div class="banner">
	<ul id="lightSlider">
		<a href="#p0">
			<div class="textos col12">
				<h2 class="tit">Entenda como funciona:</h2>
				<h3 class="sub italico bold"><strong style="padding:4%;font-size: 16px;border-radius:30px;">Clique aqui</strong></h3>
				<br>
			</div>
		</a>
		<li>
			<div class="textos col12">
				<h2 class="tit">Aluguel<br> Investido TPA</h2>
				<h3 class="sub italico bold">Use o valor da locação como <br><strong>entrada na compra do seu Apê!</strong></h3>
				<br>
			</div>
		</li>
		<li>
			<div class="textos col12">
				<h2 class="tit">A partir de R$3.180*</h2>
				<h3 class="sub italico bold"><strong>Não precisa se descapitalizar para</strong> <br>mudar para o novo apartamento </h3>
			</div>
			
		</li>
		<a href="#p2">
			<div class="textos col12">
				<h2 class="tit">Contrato de <br>30 meses</h2>
				<h3 class="sub italico bold"><strong>nos 12 primeiros meses todo o valor*</strong> <br> poderá virar 100% como entrada</h3>
			</div>
			
		</a>
		
		<a href="#p1">
			<div class="textos col12">
				<h2 class="tit">Opção de compra</h2>
				<h3 class="sub italico bold"><strong>Em até 12 meses, conforme contrato.<br></strong></h3>
			</div>
		</a>	
		
	</ul>
	<div class="bn-nav">
		<a class="anterior2"><img src="img/seta-e.svg"/></a>
		<a class="proximo2"><img src="img/seta-d.svg"/></a>
	</div>
	<div class="bg-banner">
		<div class="over-foto"></div>
		<img src="img/banner1.jpg" alt="Sala do decorado"/>
	</div>
	
</div>
<div class="form-fachada">
	<div class="col12">
		<iframe width="100%" height="560" id="myIframe"  src="https://www.youtube.com/embed/V7_21HIFP54" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		
		<div class="col-left">
			<h2 class="tit bold">Entre em contato</h2>
			<span class="subtit italico verde">Deixe seus dados abaixo para saber mais</span>
			<form method="POST" id="formContatoApi2" class="form" action="1salvabanco.php">

				<div class="formContatoMensagem">
					<a class="bt-fechar3 bg-azul redondo f32 centro branco">x</a>
					<span></span>
				</div>
				<input type="text" name="apiNome" id="name" value="" placeholder="*Nome" required>
				<input type="email" name="apiEmail" id="email" placeholder="*Email" value="" required>
				<input type="text" name="apiTelefone1" id="fone" placeholder="*Telefone" value="" class="maskphoneApi" required>
				<!--<textarea name="apiMensagem" id="mensagem" placeholder="Mensagem"></textarea>-->
				<input type="submit" class="submit verde" id="submit" value="Quero saber mais">


				<input type="hidden" name="origem" value="<?php echo $origemHost?>">

				<input type="hidden" name="device" value="<?php echo $device?>">


				<input type="hidden" name="tipo" value="contato"/>

				<input type="reset" class="resetar" style="display:none">

			</form>
		</div>
		<div class="col-right">
			<img src="img/ico-campanha.svg" class="icocampanha" alt="Icone Campanha"/>
			<span class="subtit ">Fachada do <strong>Sky Paulicéia</strong></span>
			<img src="img/fachada.jpg" class="fachada" alt="Fachada do Sky"/>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>

<div class="diferenciais">
	<div class="col12">
		<h2 class="tit bold">Diferenciais do Sky</h2>
		<ul>
			<li>
				<div class="col-left"><img src="img/car2.svg"  /></div>
				<span>Rua Brigadeiro Tobias 334 - Centro SP </span>
			</li>
			<li>
				<div class="col-left"><img src="img/car.svg"  /></div>
				<span>Fácil acesso para Marginal Tietê e Av. 9 de Julho e 23 de Maio</span>
			</li>
			<li>
				<div class="col-left"><img src="img/train.svg"  /></div>
				<span>3 min. da Estação Luz <span class="regular">(linha amarela e azul)</span></span>
			</li>
			<li>
				<div class="col-left"><img src="img/shopping-bag.svg"  /></div>
				<span>Quadrilátero das Compras <span class="regular">(entre o Bom Retiro, Santa Efigênia, 25 de Março e Brás)</span></span>
			</li>
			
		</ul>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>
<div class="carrossel">
	<div class="col12" >
		<ul id="lightSlider_">
			<li >
				<a class="lightbox" href="img/large/fachada.jpg">
					<span class="legenda">Fachada</span>
					<img src="img/fachada2.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/hall.jpg">
					<span class="legenda">Hall</span>
					<img src="img/hall.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/piscina.jpg">
					<span class="legenda">Piscina</span>
					<img src="img/piscina.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/lavanderia.jpg">
					<span class="legenda">Lavanderia</span>
					<img src="img/lavanderia.jpg"/>
				</a>
			</li>
			
			<li >
				<a class="lightbox" href="img/large/churrasqueira.jpg">
					<span class="legenda">Churrasqueira</span>
					<img src="img/churrasqueira.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/fitness.jpg">
					<span class="legenda">Fitness</span>
					<img src="img/fitness.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/salao.jpg">
					<span class="legenda">Salão de Festas</span>
					<img src="img/salao.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/terraco.jpg">
					<span class="legenda">Terraço</span>
					<img src="img/terraco.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/planta1.jpg">
					<span class="legenda">Planta 30m²</span>
					<img src="img/planta1.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/planta2.jpg">
					<span class="legenda">Planta 30m²</span>
					<img src="img/planta2.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/planta3.jpg">
					<span class="legenda">Planta 80m²</span>
					<img src="img/planta3.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/planta4.jpg">
					<span class="legenda">Planta 30m²</span>
					<img src="img/planta4.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/sala.jpg">
					<span class="legenda">Foto do decorado</span>
					<img src="img/sala.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/quarto.jpg">
					<span class="legenda">Foto do decorado</span>
					<img src="img/quarto.jpg"/>
				</a>
			</li>
			<li >
				<a class="lightbox" href="img/large/sala2.jpg">
					<span class="legenda">Foto do decorado</span>
					<img src="img/sala2.jpg"/>
				</a>
			</li>
			
		</ul>
		<a class="anterior"><img src="img/seta-verde-e.svg"/></a>
		<a class="proximo"><img src="img/seta-verde-d.svg"/></a>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="duvidas" id="duvidas">
	<div class="col12" >
		<h2 class="bold" id="p0">Dúvidas<br> Frequentes</h2>
		<br>
		<hr>
		<br>
		<p >
			
		<strong>1 -  Como funciona o contrato de 30 meses?</strong>
			R: É feito um contrato de locação convencional de 30 meses. Caso você tenha interesse em comprar o apartamento, a TPA reverte 100% do valor pago de aluguel dos primeiros 12 meses como entrada na aquisição do imóvel (o valor não contempla IPTU e condomínio). 
		</p>
		<p id="p1">
			<strong>2 -  Quando posso exercer a opção de compra?  </strong>
			R: Até 12 meses
		</p>
		<p id="p2">
			<strong>3 - Quais são os documentos necessários para análise de crédito e emissão do contrato de aluguel? </strong>

			Ficha Cadastral Preenchida e Assinada;<br>
			Documentos Pessoais - RG, CPF e comprovante de estado Civil;<br>
			Imposto de Renda; <br>
			Holerites e/ou movimentação bancária;<br>
			Comprovante de endereço; <br>
			A TPA poderá solicitar outros documentos para comprovação de renda se houver necessidade.
		</p>
		<p id="p3">
			<strong>4 - Os imóveis terão kit acabamento? </strong>
			R: Temos apartamentos com kit de acabamento (piso e box) e apartamentos sem acabamento, a critério do cliente. Lembrando que cada apartamento possui um preço para venda.

		</p>
		<p id="p4">
			<strong>5 – Quando eu receberei as chaves do imóvel?</strong>
			R: As chaves serão entregues após a vistoria.
		</p>
		<p id="p5">
			<strong>6 - Qual garantia será necessária para contratação do aluguel?</strong>
			R: Seguro fiança.
		</p>
		<p id="p6">
			<strong>7 - A partir de qual mês devo começar a pagar Condomínio e IPTU? </strong>
			R: Condomínio e IPTU deverão ser pagos a partir do momento de recebimento das chaves.
		</p>
		<p id="p7">
			<strong>8 - Devo pagar a primeira parcela de aluguel para começar a utilizar o imóvel?</strong>
			R: Não, a cobrança do primeiro aluguel será realizada após de 30 dias de uso.
		</p>
		<p id="p8">
			<strong>9 - Quais são as datas de pagamento do aluguel? </strong>
			R: Entre 1º ao 5º dia útil.
		</p>
		<p id="p9">
			<strong>10 - Posso negociar o valor do aluguel?</strong>
			R: Não.
		</p>
		<p id="p10">
			<strong>11 - O meu aluguel sofrerá reajuste?</strong>
			R: Sim, sofrerá reajuste anual por meio do índice IGP-M acumulado de 12 meses.
		</p>
		<p id="p11">
			<strong>12 - Poderei realizar benfeitorias no imóvel locado? </strong>
			R: Sim, porém antes de qualquer benfeitoria deve obter a autorização da TPA. 
		</p>
		<p id="p12">
			<strong>13 - Serei ressarcido das benfeitorias realizadas no imóvel locado se não exercer a opção de compra?</strong>
			R: Não.
		</p>
		<p id="p13">
			<strong>14 – Eu posso renovar o contrato de locação após os 30 meses de locação?</strong>
			R: Não.
		</p>
		<p id="p14">
			<strong>15 - Caso eu rescinda o contrato de aluguel durante sua vigência qual será a multa?</strong>
			R: A multa por rescisão de contrato é o valor correspondente a 3 alugueis e a cobrança é proporcional ao tempo de aluguel decorrido. Como em qualquer contrato de locação tradicional.
		</p>
		<p id="p15">
			<strong>16 - Posso antecipar a compra da unidade locada a qualquer momento?</strong>
			R: Sim.
		</p>
		<p id="p16">
			<strong>17 - Se eu desistir de comprar o imóvel, terei que deixar a unidade da forma como recebi no momento da locação?</strong>
			R: Sim, deverá ser devolvido da mesma forma que recebeu, como em qualquer contrato de locação.
		</p>
		<p id="p17">
			<strong>18 - No caso de compra poderei utilizar o meu FTGS para abater o saldo a financiar?</strong>
			R: Sim, conforme as regras de utilização do FGTS.
		</p>
		<p id="p18">
			<strong>19 - Deverei arcar com as taxas de escritura para contratação do financiamento direto ou bancário? </strong>
			R: Sim, as taxas de escritura para contratação do financiamento são de responsabilidade do comprador.
		</p>
		<p id="p19">
			<strong>20 - Posso sublocar o apartamento? </strong>
			R: Não
		</p>
	</div>
</div>
<div class="clear"></div>
<div class="rodape2">
	<div class="col12" >
		<div class="col-left vds">
			<h2><strong>VENDAS</strong>
			<a href="tel:11995896488">11 99589-6488</a>
			</h2>
		</div>
		<div class="col-left coli2">
			<span class="upper">Intermediação</span><br>
			<div><img src="img/tpa-vendas.svg" alt="Tpa Vendas Creci 033633-j"/></div>
		</div>
		<div class="col-left coli2 maior">
			<span class="upper">Incorporação e Construção</span><br>
			<div><a class="" href="http://tpaempreendimentos.com.br/"><img src="img/tpa.svg" alt="Tpa Vendas Empreendimentos"/></a></div>
		</div>
		
		
		<div class="col-left coli2 incena">
			<span class="upper">Marketing e desenvolvimento</span><br>
			<div><a class="" href="http://incenadigital.com.br/"><img src="img/logo-incena.svg" alt="Incena Digital"/></a></div>
		</div>
		<div class="clear"></div>
		<br><br><br><br>
		<span class="texto-legal">*O valor não contempla IPTU e Condomínio </span>
	</div>
</div>
<div class="form-whats over esconde-tel">
	<div class="se">
		<a class="bt-fechar f90">X</a>
		<span class="f48 centro">Por sua segurança, coloque seu nome e número de celular para falar conosco por WhatsApp.</span>
		<br><br>
		<form method="POST" id="formContatoApi" class="form" action="1salvabanco.php">

				<div class="formContatoMensagem"><span></span></div>

				<div class="col50 col-left">
					<input type="text" name="apiNome" id="name" value="" placeholder="*Nome" required>
					<input type="hidden" name="apiEmail" id="email" placeholder="*Email" value="" required>

					<input type="text" name="apiTelefone1" id="fone" placeholder="*Telefone" value="" class="maskphoneApi" required>
				</div>
				<div class="col50 col-right">
					<textarea name="apiMensagem" id="mensagem" placeholder="Mensagem"></textarea>
					<input type="submit" class="submit f13" id="submit" value="Conectar via WhatsApp">
				</div>

				<input type="hidden" name="origem" value="<?php echo $origemHost?>">

				<input type="hidden" name="device" value="<?php echo $device?>">



				<input type="reset" class="resetar" style="display:none">
				<input type="hidden" name="tipo" value="whatsapp"/>

		</form>
	</div>
</div>


</body>

<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="js/lightslider.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src=" js/jquery.form.js" type="text/javascript"></script>
<script src="js/waypoints.min.js" type="text/javascript"></script>
<script src="js/mask.js" type="text/javascript"></script>

<script src="js/lightbox/jquery.lightbox.min.js" type="text/javascript"></script>
<script src="js/site.js" type="text/javascript"></script>

<script>
$(document).ready(function(){
	var dominioOrigem = "<?php echo $origemHost; ?>";
	var dispositivo = "<?php echo $device; ?>";


    var dominioOrigem = "<?php echo $origemHost; ?>";
    var dispositivo = "<?php echo $device; ?>";
    var parametros = "<?php echo $parametros;?>";

	 var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	  },
	  spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		  }
	  };
	  jQuery(".maskphoneApi").mask(SPMaskBehavior, spOptions);

	$('#formContatoApi').on('submit',function(event){
        event.preventDefault();

        var flagErro = false;
        $('#formContatoApi input:text, #formContatoApi #fone, #formContatoApi select').each(function(){
            if($(this).val() == ''){
                $(this).css('border','1px solid red');
                flagErro = true;
            }else{
                $('.formContatoMensagem span').html('')
                $(this).css('border','');
				$('.formContatoMensagem').addClass("teste2");
				function explode(){
				  $('.formContatoMensagem').removeClass("teste2");
				}
				setTimeout(explode, 3000);
            }
        })
        if(flagErro){
            $('.formContatoMensagem').html('Existem campos em branco!');
			$('.formContatoMensagem').addClass("teste2");
			function explode(){
				  $('.formContatoMensagem').removeClass("teste2");
				}
				setTimeout(explode, 3000);
        }else{
            $.post("https://www.novocrm.atendimentoon.com.br/api/form/externo/<?php echo $empreendimentoId; ?>",  $(this).serialize()+"&apiOrigem="+dominioOrigem+"&apiDispositivo="+dispositivo+"&"+parametros , function(data){});
            event.preventDefault();
            $('.submit').prop('disabled', true);
			$(".submit").attr("disabled", true);
            $.post($(this).attr('action'), $(this).serialize() + "&"+parametros , function(data){
                $('.formContatoMensagem span').html(data);
                $('.formContatoMensagem').addClass("teste");
               
                $('.submit').prop('disabled', false);
                function explode(){
						 $('.esconde-tel ').toggle(); 
						 $('.bt-mostrar ').toggle();  
          				}
          				setTimeout(explode, 3000);
					  gtag('event', 'enviar', { 
						  'event_category': 'formulario_whats',
						  'event_label': 'whatsapp'
						});
						fbq('track', 'Lead', {
							content_name: 'form_whats_sky_aluguel'
						});
						location.href ="https://api.whatsapp.com/send?phone=5511995896488";

            });
		}

	});
	$('#formContatoApi2').on('submit',function(event){
        event.preventDefault();


        var flagErro = false;
        $('#formContatoApi2 input:text, #formContatoApi2 input:[type=email], #formContatoApi2 #fone , #formContatoApi2 select').each(function(){
            if($(this).val() == ''){
                $(this).css('border','1px solid red');
                flagErro = true;
            }else{
                $('.formContatoMensagem span').html('')
                $(this).css('border','');
				$('.formContatoMensagem').addClass("teste2");
				function explode(){
				  $('.formContatoMensagem').removeClass("teste2");
				  $("#formContatoApi2")[0].reset();

				}
				setTimeout(explode, 3000);
            }
        })
        if(flagErro){
            $('.formContatoMensagem').html('Existem campos em branco!');
			$('.formContatoMensagem').addClass("teste");
			function explode(){
				  $('.formContatoMensagem').removeClass("teste");
				}
				setTimeout(explode, 3000);
        }else{
            $.post("https://www.novocrm.atendimentoon.com.br/api/form/externo/<?php echo $empreendimentoId; ?>",  $(this).serialize()+"&apiOrigem="+dominioOrigem+"&apiDispositivo="+dispositivo+"&"+parametros , function(data){});
            event.preventDefault();
            $('.submit').prop('disabled', true);
			$(".submit").attr("disabled", true);
            $.post($(this).attr('action'), $(this).serialize() + "&"+parametros , function(data){
                $('.formContatoMensagem span').html(data);
                $('.formContatoMensagem').addClass("teste");
				function explode(){
				  $('.formContatoMensagem').removeClass("teste");
				}
				setTimeout(explode, 3000);
                
                $('.submit').prop('disabled', false);
                
					gtag('event', 'enviar', { 
						  'event_category': 'formulario',
						  'event_label': 'fale_conosco'
					});
					fbq('track', 'Lead', {
						content_name: 'form_sky_aluguel'
					});
		 					
            });
			//aqui deveria começar a integração da ANAPRO porém está enviando mais de uma vez e logo em seguida dá erro
				

				var nome = $("#formContatoApi2 input:text").val(),
					  telefone = $("#formContatoApi2 #fone").val().replace(/[^\d]+/g, ''),
					  email = $("#formContatoApi2 input:[type=email]").val(),
					  dados = {
						  
						  "Key": "FudObOXhOLA1",
						  "TagAtalho": "",
						  "CampanhaKey": "sZ8usFGGN-c1",
						  "ProdutoKey": "DxDB_yX6nFE1",
						  "CanalKey": "mOsK64u4x-k1",
						  "Midia": "Landing page Sky Aluguel",
						  "Peca": "",
						  "GrupoPeca": "",
						  "CampanhaPeca": "",
						  "PessoaNome": nome,
						  "PessoaSexo": "",
						  "PessoaEmail": email,
						  "PessoaTelefones": [{
							  "Tipo": "OUTR",
							  //"DDD": ddd,
							  "Numero": telefone,
							  "Ramal": null
						  }],
						  "Status": "",
						  "KeyExterno": "",
						  "KeyIntegradora": "666AED32-B25E-4FD9-8F27-9A1076A52518",
						  "KeyAgencia": "79e5ccab-7046-4735-8f43-0633edb05ebd"
					  };
							

				 $.ajax({
					  url: 'https://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect',
					  data: dados,
					  crossDomain: true,
					  cache: false,
					  type: 'POST',
					  dataType: 'json',
					  success: function (response) {
						  console.log('Response', response);
						 // $("#formContatoApi2").submit();
						}
					  });
					  
				// termina integração ANAPRO
					  
		}

	});
	$('#formContatoApi3').on('submit',function(event){
        event.preventDefault();

        var flagErro = false;
        $('#formContatoApi3 input:text, #formContatoApi3 input:[type=email], #formContatoApi3 #fone , #formContatoApi3 select').each(function(){
            if($(this).val() == ''){
                $(this).css('border','1px solid red');
                flagErro = true;
            }else{
                $('.formContatoMensagem span').html('')
                $(this).css('border','');
				$('.formContatoMensagem').addClass("teste2");
				function explode(){
				  $('.formContatoMensagem').removeClass("teste2");
				}
				setTimeout(explode, 3000);
            }
        })
        if(flagErro){
            $('.formContatoMensagem').html('Existem campos em branco!');
			$('.formContatoMensagem').addClass("teste");
			function explode(){
				  $('.formContatoMensagem').removeClass("teste");
				}
				setTimeout(explode, 3000);
        }else{
            $.post("https://www.novocrm.atendimentoon.com.br/api/form/externo/<?php echo $empreendimentoId; ?>",  $(this).serialize()+"&apiOrigem="+dominioOrigem+"&apiDispositivo="+dispositivo+"&"+parametros , function(data){});
            event.preventDefault();
            $('.submit').prop('disabled', true);
			$(".submit").attr("disabled", true);
            $.post($(this).attr('action'), $(this).serialize() + "&"+parametros , function(data){
                $('.formContatoMensagem span').html(data);
                $('.formContatoMensagem').addClass("teste");
                //$("#formContatoApi3").reset();
                $('.submit').prop('disabled', false);
                function explode(){
					$('.esconde-tel2 ').toggle(); 
		  			 $('.bt-mostrar2 ').toggle();  
          				}
          				setTimeout(explode, 3000);
					gtag('event', 'enviar', { 
						'event_category': 'formulario',
						'event_label': 'formulario casa pronta'
					});
					fbq('track', 'Lead', {
						content_name: 'formulario casa pronta'
					});
            });
		}

	});

});
</script>



</html>
