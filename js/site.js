// JavaScript Document

$(document).ready(function() {
						   
		var slider = $("#lightSlider_").lightSlider({	
			//LAZY LOADING
			onBeforeStart: function (slider) {
				for (var i = 1; i <= 2; i++) {
					var img = slider.find('li:nth-child(' + i + ') img');
					img.attr('src', img.attr('data-src'));
				}
			},
			onSliderLoad: function (slider) {
				$('.aboutSliderHidden').removeClass('aboutSliderHidden');
				slider.parent().find('.lSPrev').show();
			},
			onAfterSlide: function (slider, scene) {
				for (var i = 0; i <= 10; i++) {
					var img = slider.find('img').eq(slider.getCurrentSlideCount() + i);
					img.attr('src', img.attr('data-src'));
				}

				var lsPrev = slider.parent().find('.lSPrev');
				var lsNext = slider.parent().find('.lSNext');

				if (slider.getCurrentSlideCount() === 1) {
					lsPrev.hide();
				}
				else if (slider.getCurrentSlideCount() === slider.find('li').length) {
					lsNext.hide();
				}
				else {
					lsPrev.show();
					lsNext.show();
				}
			},
			 slideMove: 1,
			 item:2,
			autoWidth: false,
			slideMargin: 0,
			adaptiveHeight:true,
			 auto: false,
             loop: false,
			 responsive : [
				{
					breakpoint:480,
					settings: {
						item:1,
						slideMove:1
					  }
				}
			]
		 }); 

		 $('.proximo').click(function(){
			slider.goToNextSlide();    
		 });
		  $('.anterior').click(function(){
			slider.goToPrevSlide();    
		 });
		 
		var slider2 = $("#lightSlider").lightSlider({					   
			slideMove: 1,
			 item:1,
			autoWidth: false,
			slideMargin: 0,
			adaptiveHeight:true,
			pause:4000,
			 auto: true,
             loop: true,
			 responsive : [
				{
					breakpoint:480,
					settings: {
						item:1,
						slideMove:1
					  }
				}
			]
		 }); 
		 $('.proximo2').click(function(){
			slider2.goToNextSlide();    
		 });
		  $('.anterior2').click(function(){
			slider2.goToPrevSlide();    
		 });

		 
		$('.lightbox').lightbox({ 
			maxheight: 1200,
			maxwidth: 1900 
		});
		
		if ($(window).width() < 600) {
			//alert('menor que 600px');
		  // $('.carrossel .lightbox').removeAttr( "href" );
		  // $('.lightbox').removeClass( "lightbox" );
	   }
		   
	
		jQuery(document).ready(function($) { 
			$(".scroll").click(function(event){        
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
		   });
		});
			
		 $('.tele').click(function(){
			 $('.esconde-tel ').toggle(); 
			$('.bt-mostrar ').toggle();  
			 
		 });
		 $('.casas').click(function(){
			$('.esconde-tel2 ').toggle(); 
		   $('.bt-mostrar2 ').toggle();  
		  
		});
		 
		 $('.bt-fechar').click(function(){
			$('.esconde-tel ').toggle(); 
			 $('.bt-mostrar ').toggle();  			 
		 });

		 $('.bt-fechar2').click(function(){
			$('.esconde-tel2 ').toggle(); 
		    $('.bt-mostrar2 ').toggle();    			 
		});
		$('.bt-fechar3').click(function(){
		   $('.formContatoMensagem').removeClass("teste");
		   $("#formContatoApi2")[0].reset();  			 
		});
		
		$(".form").styleForm();
		
		
		 
		
		 
		
	});