<?php
require_once dirname(__FILE__).'/../Conexao/Conexao.php';
require_once dirname(__FILE__).'/../Entity/SortearEmail.php';
require_once dirname(__FILE__).'/../Log/GeraLog.php';
class DaoSortearEmail {

	public static $instance;


	public static function getInstance()
	{
		if (!isset(self::$instance))
			self::$instance = new DaoSortearEmail();

			return self::$instance;
	}


	public function BuscarChatDaVez() {
			try {
					$sql = "SELECT * FROM emails WHERE sorteado = 1";
					$p_sql = Conexao::getInstance()->prepare($sql);
					$p_sql->execute();
					return $this->populaSortearEmail($p_sql->fetch(PDO::FETCH_ASSOC));
			} catch (Exception $e) {
					GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage());
			}
	}

    public function BuscarEmailDaVez($proximo = true) {
        try {
            $sql = "SELECT * FROM emails WHERE sorteado = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            $emailDaVez = $this->populaSortearEmail($p_sql->fetch(PDO::FETCH_ASSOC));
						if($proximo)
            	$this->RemoverEmailDaVez($emailDaVez);
            return $emailDaVez;
        } catch (Exception $e) {
            GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage());
        }
    }
	private function populaSortearEmail($row) {
	        $pojo = new SortearEmail;
	        $pojo->setId($row['id']);
	        $pojo->setEmail($row['email']);
			$pojo->setTipo($row['tipo']);
	        $pojo->setSorteado($row['sorteado']);
			$pojo->setLinkChat($row['link_chat']);
			$pojo->setAssunto($row['assunto']);
	        return $pojo;
	    }

    private function RemoverEmailDaVez(SortearEmail $emailDaVez) {
         try {
            $sql = "UPDATE emails set sorteado = 0 WHERE id = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $emailDaVez->getId());
            $p_sql->execute();

            $this->ProximoEmail($emailDaVez);
        } catch (Exception $e) {
            GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage());
        }
    }

	private function atualizaSort($id = null){
		$sql = "UPDATE emails e JOIN(SELECT id, MIN(id) FROM emails WHERE id > :id) idFirst ON e.id = idFirst.id SET e.sorteado = 1";
		if(is_null($id)){
			$sql = "UPDATE emails e JOIN(SELECT id, MIN(id) FROM emails) idFirst ON e.id = idFirst.id SET e.sorteado = 1";
		}

		$p_sql = Conexao::getInstance()->prepare($sql);
		$p_sql->bindValue(":id", $id);
		$p_sql->execute();
	}

    private function ProximoEmail(SortearEmail $emailDaVez)
    {
    	$sql = "SELECT MAX(ID) AS ultimo_id FROM emails";
    	$db = Conexao::getInstance();
    	$p_sql = $db->prepare($sql);
    	$p_sql->execute();
    	$ultimoId = $p_sql->fetch(PDO::FETCH_ASSOC);
    	if($emailDaVez->getId() == $ultimoId['ultimo_id']){
			$this->atualizaSort();
    	}else{
    		$this->atualizaSort($emailDaVez->getId());
    	}
    }

}
