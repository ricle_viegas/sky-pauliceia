<?php
require_once dirname(__FILE__).'/../Conexao/Conexao.php';
require_once dirname(__FILE__).'/../Entity/Contato.php';
require_once dirname(__FILE__).'/../Log/GeraLog.php';
class DaoContato
{
	public static $instance;


	public static function getInstance()
	{
		if (!isset(self::$instance))
			self::$instance = new DaoContato();

			return self::$instance;
	}

	public function Inserir(Contato $contato) {
		try {
			$sql = "INSERT INTO contato (
                nome,
                email,
                telefone,
                manha,
                tarde,
                noite,
                origem,
                device,
                created_at
								)
                VALUES (
                :nome,
                :email,
                :telefone,
                :manha,
                :tarde,
                :noite,
                :origem,
                :device,
                NOW()
					)";

			$p_sql = Conexao::getInstance()->prepare($sql);

			$p_sql->bindValue(":nome", $contato->getNome());
			$p_sql->bindValue(":email", $contato->getEmail());
			$p_sql->bindValue(":telefone", $contato->getTelefone());
            $p_sql->bindValue(":manha", $contato->getManha());
            $p_sql->bindValue(":tarde", $contato->getTarde());
            $p_sql->bindValue(":noite", $contato->getNoite());
			$p_sql->bindValue(":origem", $contato->getOrigem());
			$p_sql->bindValue(":device", $contato->getDevice());

			return $p_sql->execute();
		} catch (Exception $e) {
			GeraLog::getInstance()->inserirLog("Erro: Código: " .$e->getCode() . " Mensagem: " . $e->getMessage());
		}
	}


}
