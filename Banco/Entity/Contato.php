<?php
class Contato {
	private $id;
	private $nome;
	private $email;
	private $telefone;
	private $manha;
	private $tarde;
	private $noite;
	private $origem;
	private $device;
	private $createdAt;


	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getNome()
	{
		return $this->nome;
	}

	public function setNome($nome)
	{
		$this->nome = $nome;
		return $this;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	public function getTelefone()
	{
		return $this->telefone;
	}

	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;
		return $this;
	}

    /**
     * @return mixed
     */
    public function getManha()
    {
        return $this->manha;
    }

    /**
     * @param mixed $manha
     */
    public function setManha($manha)
    {
        $this->manha = $manha;
    }

    /**
     * @return mixed
     */
    public function getTarde()
    {
        return $this->tarde;
    }

    /**
     * @param mixed $tarde
     */
    public function setTarde($tarde)
    {
        $this->tarde = $tarde;
    }

    /**
     * @return mixed
     */
    public function getNoite()
    {
        return $this->noite;
    }

    /**
     * @param mixed $noite
     */
    public function setNoite($noite)
    {
        $this->noite = $noite;
    }

	public function getOrigem()
	{
		return $this->origem;
	}

	public function setOrigem($origem)
	{
		$this->origem = $origem;
		return $this;
	}

	public function getDevice()
	{
		return $this->device;
	}

	public function setDevice($device)
	{
		$this->device = $device;
		return $this;
	}

	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	public function returnObj()
	{
		return $this;
	}
}
