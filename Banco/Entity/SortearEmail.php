<?php
class SortearEmail
{
	private $id;
	private $email;
	private $tipo;
	private $sorteado;
	private $linkChat;
	private $assunto;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	public function getTipo()
	{
		return $this->tipo;
	}

	public function setTipo($tipo)
	{
		$this->tipo = $tipo;
		return $this;
	}

	public function getSorteado()
	{
		return $this->sorteado;
	}

	public function setSorteado($sorteado)
	{
		$this->sorteado = $sorteado;
		return $this;
	}

	public function getEmailArray()
	{
		$email = $this->getEmail();
		if(empty($email))
			return false;
		return explode(',', $email);
	}

    /**
     * @return mixed
     */
    public function getLinkChat()
    {
        return $this->linkChat;
    }

    /**
     * @param mixed $linkChat
     */
    public function setLinkChat($linkChat)
    {
        $this->linkChat = $linkChat;
    }


	/**
	 * Get the value of assunto
	 */ 
	public function getAssunto()
	{
		return $this->assunto;
	}

	/**
	 * Set the value of assunto
	 *
	 * @return  self
	 */ 
	public function setAssunto($assunto)
	{
		$this->assunto = $assunto;

		return $this;
	}
}
